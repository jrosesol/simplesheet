import django
from datetime import datetime
from django.db import models
from mockbackend.models import Company, Person, Project, Task

def populate():
    # Create Company
    acme = Company(name='ACME')
    acme.save()
    
    #Create a Boss working in ACME and an employee
    acme = Company.objects.get(name='ACME')
    boss = Person(name='Elmer Fudd',
                  email='ef@acme.com',
                  role='Boss')
    boss.company = acme
    boss.save()
    
    employee = Person(name='Yosemite Sam',
                  email='ys@acme.com',
                  role='Employee')
    employee.company = acme
    employee.save()

    project_rabbit = Project(title = 'Catch Rabbit')
    project_rabbit.owner = Person.objects.get(name='Elmer Fudd')
    project_rabbit.save()
    
    #Create task
    build_trap_start = datetime(2011, 12,10, 8,30,00)
    build_trap_end = datetime(2011,12,10, 15,30,00)
    project_rabbit = Project.objects.get(title = 'Catch Rabbit')
    build_trap = Task(title = 'Build a trap',
                      details = '',
                      allDay = False,
                      unTimed = False)
    build_trap.startTime = build_trap_start
    build_trap.endTime = build_trap_end
    build_trap.project = project_rabbit
    build_trap.save()
    build_trap = Task.objects.get(id=1)
    build_trap.persons.add(Person.objects.get(name = 'Yosemite Sam'))
    build_trap.persons.add(Person.objects.get(name='Elmer Fudd'))
    
