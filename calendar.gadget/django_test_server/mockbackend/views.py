# Create your views here.

from django.http import HttpResponse
from django.template import Context, loader
from models import Person

def gadget(request):
    
    persons = Person.objects.all().order_by('name')
    t = loader.get_template('index.html')
    c = Context({
        'persons': persons,
    })
    return HttpResponse(t.render(c))
    

def process_jsonp(request):
    """
    Accept asyncronous GET requests to validate forms. The send back a JSON
    response containing any errors.
    """
    print request.GET
    callback = request.GET['sc_callback']
    q = request.GET['q']
    if q == 'jsonp':    
        response = {'Name' : request.GET['Name']}
    else:
        response = {'Name' : 'Nobody'}
        
    return JsonpResponse(response, callback)


import json
 
class JsonpResponse(HttpResponse):
    def __init__(self, data, callback):
        jsond = json.dumps(data)
        jsonp = "%s(%s)" % (callback, jsond)
        HttpResponse.__init__(
            self, jsonp,
            mimetype='application/json'
        )

