from django.db import models

# Create your models here.


class Company(models.Model):
    """ There should be a user admin for the Company """
    name = models.CharField(max_length = 127)
    

class Person(models.Model):
    company = models.ForeignKey(Company)
    name = models.CharField(max_length = 255)
    email = models.CharField(max_length = 255)
    role = models.CharField(max_length = 255)

class Project(models.Model):
    owner = models. ForeignKey(Person)
    title = models.CharField(max_length = 255)


class Task(models.Model):
    project = models. ForeignKey(Project)
    persons = models.ManyToManyField(Person)
    title = models.CharField(max_length = 255)
    details = models.CharField(max_length = 255)
    allDay = models.BooleanField()
    unTimed = models.BooleanField()
    startTime = models.DateTimeField()
    endTime = models.DateTimeField()
    